#lang scheme
(define (calculate-monthly-payment home-price down-payment-percent loan-term-years interest-rate)

  ;; Constant property tax rate = T
  (define property-tax-rate 0.02) ; 2% property tax rate
  
  ;; Calculate the loan amount = P
  (define down-payment-amount (* home-price (/ down-payment-percent 100)))
  (define loan-amount (- home-price down-payment-amount))
  
  ;; Calculate the monthly interest rate = r
  (define monthly-interest-rate (/ interest-rate 12 100))
  
  ;; Calculate the number of monthly payments = n
  (define number-of-payments (* loan-term-years 12))
  
  ;; Calculate the monthly property tax = T
  (define monthly-property-tax (* home-price property-tax-rate 1/12))
  
  ;; Calculate the monthly mortgage payment -> M = P * ([r(1+r)^n]/[(1+r)^n - 1] + T
  (define numerator-value (* loan-amount monthly-interest-rate (expt (+ 1 monthly-interest-rate) number-of-payments)))
  (define denominator-value (- (expt (+ 1 monthly-interest-rate) number-of-payments) 1))
  (define monthly-payment (/ numerator-value denominator-value))
  
  ;; Calculate the monthly principal and interest components
  (define principal-component (- monthly-payment monthly-property-tax))
  (define interest-component (- monthly-payment principal-component))

  ;; Round off to two decimal places
  (define (round-off given-value)
  (if (number? given-value)
      (let ((rounded (* 100.0 given-value))) ; Multiply by 100 to get the two decimal places
        (/ (round rounded) 100.0))     ; Divide by 100 to get rounded final
      (error "The input given by the user is not a number :((")))

  (define rounded-principal (round-off principal-component))
  (define rounded-interest (round-off interest-component))
  (define rounded-property-tax (round-off monthly-property-tax))
  (define rounded-monthly-pay (round-off (+ monthly-payment monthly-property-tax)))

 ;; Display the result
  (newline)
  (display "~~~~~~~~~~~~~~~~~~~~~~~MONTHLY MORTGAGE~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
  (display "Payment Breakdown:\n")
  (display (format "Principal: $~a\n" rounded-principal))
  (display (format "Interest: $~a\n" rounded-interest))
  (display (format "Property Tax: $~a\n" rounded-property-tax))
  (display (format "Your Total Monthly Payment: $~a\n" rounded-monthly-pay))
  (display "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
  (newline))

(define (record-transaction transactions category amount)
  (cons (cons category amount) transactions))

(define (calculate-total amount-list)
  (foldl (lambda (item total) (+ (cdr item) total)) 0 amount-list))

(define (print-transactions transactions)
  (display "Category\tAmount\n")
  (for-each (lambda (item)
              (display (car item))
              (display "\t")
              (display (cdr item))
              (newline))
            transactions))

(define (menu)
  (display "Welcome to the Financial Toolkit!\n")
  (display "Choose an option:\n")
  (display "1. Mortgage Calculator\n")
  (display "2. Budget Calculator\n")
  (display "3. Quit\n")

  (let ((choice (read)))
    (cond
      ((= choice 1)
       (display "You chose Mortgage Calculator.\n")
       (get-mortgage-input)
       (menu))
      ((= choice 2)
       (display "You chose Budget Calculator.\n")
       (student-budget)
       (menu))
      ((= choice 3)
       (display "Goodbye!\n"))
      (else
       (display "Invalid choice. Please select 1, 2, or 3.\n")
       (menu)))))

(define (get-mortgage-input)
  (display "Enter Home Price: $")
  (define home-price (read))
  (display "Enter Down Payment (% of Home Price): ")
  (define down-payment-percent (read))
  (display "Enter Loan Term (in years - 10, 15, 20, or 30): ")
  (define loan-term-years (read))
  (display "Enter Annual Interest Rate (%): ")
  (define interest-rate (read))

  (cond
  ((or (not (integer? loan-term-years)) (not (member loan-term-years '(10 15 20 30))))
   (display "Invalid loan term. Please enter a valid term (10, 15, 20, or 30 years)."))
  (else
   (calculate-monthly-payment home-price down-payment-percent loan-term-years interest-rate))))

(define (student-budget)
  (define transactions '())

  (display "Student Budget Management Program\n")
  (display "Enter 'q' to quit\n")

  (let loop ()
    (display "Enter transaction category (e.g., 'Food'): ")
    (define category (read-line))

    (if (string=? category "q")
        (begin
          (display "Budget summary:\n")
          (display "----------------\n")
          (display "Income:\t")
          (let ((income (calculate-total (filter (lambda (item) (>= (cdr item) 0)) transactions))))
            (display income))
          (newline)
          (display "Expenses:\t")
          (let ((expenses (calculate-total (filter (lambda (item) (< (cdr item) 0)) transactions))))
            (display expenses))
          (newline)
          (display "Balance:\t")
          (let ((balance (+ (calculate-total (filter (lambda (item) (>= (cdr item) 0)) transactions))
                            (calculate-total (filter (lambda (item) (< (cdr item) 0)) transactions)))))
            (display balance))
          (newline)
          (display "----------------\n")
          (print-transactions transactions))
        (begin
          (display "Enter transaction amount (positive for income, negative for expenses): ")
          (let ((amount (string->number (read-line)))) ; Use 'let' for local binding
            (if (number? amount)
                (begin
                  (set! transactions (record-transaction transactions category amount))
                  (display "Transaction recorded:\n")
                  (display "Category: ")
                  (display category)
                  (newline)
                  (display "Amount: ")
                  (display amount)
                  (newline)
                  (loop))
                (begin
                  (display "Invalid input. Please enter a valid number.\n")
                  (loop))))))))

(menu)