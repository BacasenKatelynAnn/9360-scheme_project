#lang scheme
;Different way to define function/method

;first way
(define (sum1 x y) (+ x y))

;second way
(define sum2 (lambda (x y) (+ x y)))

;third way unnamed Function
(display "Unnamed function")
(newline)
((lambda (x y) (+ x y))10 2)

(display "First way of defining function")
(newline)
(sum1 3 5)
(display "Second way of defining function")
(newline)
(sum2 1 2)