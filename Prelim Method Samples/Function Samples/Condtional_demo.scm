#lang scheme
;Simple Conditionals
(if (< 6 5) ;test
  "The first value is less than the second value." ;consequent
  "The first value is not less than the second value.");alternate
(newline)

(if (= 4 5);test
  "The first value is equal to the second value.";consequent
  "The first value is not equal to the second value.");alternate
(newline)

;Compound Conditions
(if (and (= 5 5) (< 5 5));test
  "Both conditions are true.";consequent
  "At least one condition is false.");alternate
(newline)

(if (or (= 5 4) (< 4 5));test
  "At least one condition is true.";consequent
  "Both conditions are false.");alternate
(newline)

;negation
(if (not (= 5 5));test
  "The condition is true.";consequent
  "The condition is false.");alternate
(newline)

;Nested Condition
(define (grade student-score)
  (cond
    ((> student-score 90) "A")
    ((and (> student-score 80) (<= student-score 89)) "B")
    ((and (> student-score 70) (<= student-score 79)) "C")
    ((and (> student-score 60) (<= student-score 69)) "D")
    ((<= student-score 60) "F")))

(grade 65)
