package Fundamentals;

/**
 * Raising tortoise
 */
public class TortoiseRacing {
    /**
     * @param v1
     * @param v2
     * @param g
     * @return time result for race
     */
  public static int[] race(int v1, int v2, int g) {
         if(v1 >= v2){
            return null;
        }
            int sec = (3600 * g) / (v2 - v1);
            int[] timeRequired = new int[3];
            timeRequired[0] = sec/3600;
            timeRequired[1] = (sec - timeRequired[0] * 3600)/60;
            timeRequired[2] = (sec - timeRequired[0] * 3600 - timeRequired[1] * 60);
        return timeRequired;
    }
}