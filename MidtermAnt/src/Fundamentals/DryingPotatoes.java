package Fundamentals;

/**
 * potato weights
 */
class DryingPotatoes {

    /**
     * @param p0
     * @param w0
     * @param p1
     * @return weight
     */
    public static int potatoes(int p0, int w0, int p1) {
       int finalWeight = w0 * (100 - p0)  / (100 - p1);
        return finalWeight;
    }
}
