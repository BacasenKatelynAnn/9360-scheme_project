package Fundamentals;

/**
 * Create a function that takes an integer as an argument and returns "Even" for even numbers or "Odd" for odd numbers.
 */
public class EvenOdd{
    /**
     * @param number
     * @return odd or even
     */
public static String evenOdd( int number){
if(number % 2 == 0){
return "even";
} 
else return "odd";
}
}
