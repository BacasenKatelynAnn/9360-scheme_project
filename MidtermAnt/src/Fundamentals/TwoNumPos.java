package Fundamentals;

/**
 * Two numbers are positive
 *
 * Description:
 *
 *
 * Task:
 * Your job is to write a function, which takes three integers a, b, and c as arguments, and returns True if exactly two of of the three integers are positive numbers (greater than zero), and False - otherwise.
 *
 * Examples:
 * two_are_positive(2, 4, -3) == True
 * two_are_positive(-4, 6, 8) == True
 * two_are_positive(4, -6, 9) == True
 * two_are_positive(-4, 6, 0) == False
 * two_are_positive(4, 6, 10) == False
 * two_are_positive(-14, -3, -4) == False
 */
public class TwoNumPos {
    /**
     * @param a
     * @param b
     * @param c
     * @return if two are positive
     */
    public static boolean twoArePositive(int a, int b, int c) {
        int check = 0;
        if(a > 0){
            check+=1;
        }
        if(b > 0){
            check+=1;
        }
        if(c > 0){
            check+=1;
        }
        if (check == 2){
            return true;
        }
        else return false;
    }

}