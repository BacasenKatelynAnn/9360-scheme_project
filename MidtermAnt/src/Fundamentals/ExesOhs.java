package Fundamentals;

import java.util.HashMap;

/**
 * Exes and Ohs
 *
 * Description :
 *
 * Check to see if a string has the same amount of 'x's and 'o's. The method must return a boolean and be case insensitive. The string can contain any char.
 *
 * Examples input/output:
 *
 * XO("ooxx") => true
 * XO("xooxx") => false
 * XO("ooxXm") => true
 * XO("zpzpzpp") => true // when no 'x' and 'o' is present should return true
 * XO("zzoo") => false
 */
public class ExesOhs {
    /**
     * @param str
     * @return boolean if x or o
     */
    public static boolean getXO(String str) {
        String converted = str.toLowerCase();
        HashMap<Character, Integer> hash = new HashMap<>();
        hash.put('x', 0);
        hash.put('o', 0);
        for (int x = 0; x < converted.length(); x++) {
            char currentChar = converted.charAt(x);
            if (hash.containsKey(currentChar)) {
                hash.put(currentChar, hash.get(currentChar) + 1);
            }
        }
        return hash.get('x').equals(hash.get('o'));
    }
}
