package Algorithm;

/**
 * Returns sum of digits
 */
public class Sum {
    /**
     * @param n
     * @return sum of digits
     */
   public static int digital_root(int n) {
        if(n != 0 && n % 9 == 0){
            return 9;
        }
        else {
            return n % 9;
        }
    }
}

