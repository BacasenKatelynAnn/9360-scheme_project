package Algorithm;


/**
 * BitCount class
 *
 */
public class BitCount {

    /**
     * @param n
     * @return bit count
     */
	public static int countBits(int n){
   int count = 0;
        while (n > 0) {
            count += n & 1; // Add the least significant bit to the count
            n >>= 1; // Right-shift n to check the next bit
        }
        return count;

    }
	
}
