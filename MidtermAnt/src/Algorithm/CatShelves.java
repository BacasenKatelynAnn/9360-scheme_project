package Algorithm;

/**
 * CatShelves class
 *
 * An infinite number of shelves are arranged one above the other in a staggered fashion.
 * The cat can jump either one or three shelves at a time: from shelf i to shelf i+1 or i+3 (the cat cannot climb on the shelf directly above its head), according to the illustration:
 *
 */
public class CatShelves{
    /**
     * @param start
     * @param finish
     * @return jumps measure
     */
public static int catAndShelves( int start, int finish)
{
int jumps = finish -start;

return (int)Math.floor(jumps / 3) + (jumps % 3);
}
}
