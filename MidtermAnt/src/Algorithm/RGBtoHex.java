package Algorithm;

/**
 * When working with color values it can sometimes be useful to extract the individual red, green, and blue (RGB) component values for a color. Implement a function that meets these requirements:
 *
 * Accepts a case-insensitive hexadecimal color string as its parameter (ex. "#FF9933" or "#ff9933")
 * Returns a Map<String, int> with the structure {r: 255, g: 153, b: 51} where r, g, and b range from 0 through 255
 * Note: your implementation does not need to support the shorthand form of hexadecimal notation (ie "#FFF")
 *
 * Example
 * "#FF9933" --> {r: 255, g: 153, b: 51}
 */
public class RGBtoHex{
    /**
     * @param r
     * @param g
     * @param b
     * @return rgp hex color
     */
    public static String rgb(int r, int g, int b) {
        r = Math.min(255, Math.max(0, r));
        g = Math.min(255, Math.max(0, g));
        b = Math.min(255, Math.max(0, b));

        String redVal = String.format("%02X", r);
        String greenVal = String.format("%02X", g);
        String blueVal = String.format("%02X", b);

        String finalHex = redVal+greenVal+blueVal;
        return finalHex;
    }

}