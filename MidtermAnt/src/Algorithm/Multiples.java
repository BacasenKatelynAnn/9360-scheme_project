package Algorithm;

import static java.util.stream.IntStream.range;
public class Multiples {

  /**
   * @param number
   * @return multiples of 3 and 5
   */
  public int solution(int number) {
  return range(3, number).filter(i->i % 3==0|| i % 5 == 0).sum();  
  }
}

