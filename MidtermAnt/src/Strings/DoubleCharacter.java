package Strings;

/**
 * Given a string, you have to return a string in which each character (case-sensitive) is repeated once.
 *
 * Examples (Input -> Output):
 * * "String"      -> "SSttrriinngg"
 * * "Hello World" -> "HHeelllloo  WWoorrlldd"
 * * "1234!_ "     -> "11223344!!__  "
 */
public class DoubleCharacter{

  /**
   * @param args string
   */
  public static void main (String [] args){
    doubleChar("sheena");
}

  /**
   * @param s
   * @return double chars
   */
  public static String doubleChar(String s){
    String mT = "";
    for (int i = 0; i<= s.length() - 1; i++) {
      mT += s.charAt(i);
      mT += s.charAt(i);
    }
  
    return mT;
  }
}
