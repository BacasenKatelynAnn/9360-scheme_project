package Strings;


/**
 * Eliminate the intruders! Bit manipulation
 *
 * Description:
 *
 * You are given a string representing a number in binary. Your task is to delete all the unset bits in this string and return the corresponding number (after keeping only the '1's).
 *
 * In practice, you should implement this function:
 *
 * public long eliminateUnsetBits(String number);
 * Examples
 * eliminateUnsetBits("11010101010101") ->  255 (= 11111111)
 * eliminateUnsetBits("111") ->  7
 * eliminateUnsetBits("1000000") -> 1
 * eliminateUnsetBits("000") -> 0
 */
public class EliminateTheIntruders {


  /**
   * @param number
   * @return bits
   */
  public static long eliminateUnsetBits(String number) {
    if (number.length() > 0){
      String finBitStr = number.replace("0", "");
      return Long.parseLong(finBitStr, 2);
    }
    else if (number.length() == 0){
      String finBitStr = "0";
      return Long.parseLong(finBitStr, 2);
    }
    return 0;
  }
}
