package Strings;

/**
 * making str to uppercase
 */
class UpperCase{
  /**
   * @param str
   * @return upper cased letter
   */
  public static String MakeUpperCase(String str){
    return str.toUpperCase();
  }
}
