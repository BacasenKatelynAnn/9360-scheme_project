package Strings;

/**
 * Isograms
 *
 * Description:
 *
 * An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a function that determines whether a string that contains only letters is an isogram. Assume the empty string is an isogram. Ignore letter case.
 *
 * Example: (Input --> Output)
 *
 * "Dermatoglyphics" --> true "aba" --> false "moOse" --> false (ignore letter case)
 */
public class Isograms {
  /**
   * @param str
   * @return boolean
   */
  public static boolean isIsogram(String str) {
    String lowered = str.toLowerCase();
    for (int x = 0; x < str.length(); x++) {
      for (int y = 0; y < str.length(); y++) {
      if (y == x) continue;
      if (String.valueOf(lowered.charAt(x)).equals(String.valueOf(lowered.charAt(y)))) return false;
    }
  }
    return true;
  }
}
