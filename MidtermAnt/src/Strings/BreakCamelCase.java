package Strings;

/**
 * Break camelCase
 *
 * DESCRIPTION:
 *
 * Complete the solution so that the function will break up camel casing, using a space between words.
 *
 * Example
 * "camelCasing"  =>  "camel Casing"
 * "identifier"   =>  "identifier"
 * ""             =>  ""
 */
public class BreakCamelCase {
    /**
     * @param input
     * @return camelcase string
     */
  public static String camelCase(String input) {
StringBuilder result = new StringBuilder();

        for (char c : input.toCharArray()) {
            if (Character.isUpperCase(c)) {
                result.append(" ");
            }
            result.append(c); 
          
    }
    return result.toString();  
  }
}
