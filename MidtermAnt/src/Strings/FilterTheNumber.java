package Strings;

/**
 * Description:
 *
 * Oh, no! The number has been mixed up with the text. Your goal is to retrieve the number from the text, can you return the number back to its original state?
 *
 * Task
 * Your task is to return a number from a string.
 *
 * Details
 * You will be given a string of numbers and letters mixed up, you have to return all the numbers in that string in the order they occur.
 */
public class FilterTheNumber{
    /**
     * @param value
     * @return filtered number
     */
 public static long filterString(final String value) {
        String finStr = "";
        for(int i = 0; i < value.length(); i++){
            if (Character.isDigit(value.charAt(i)))
                finStr +=value.charAt(i);
        }
        return Long.valueOf(finStr);
    }
 }