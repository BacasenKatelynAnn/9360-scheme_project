#lang scheme
; GetpHLevel.scm

(provide check-ph-level get-ph-level)

(define (check-ph-level)
  (define ph (get-ph-level))
  (if (and (>= ph 6.8) (<= ph 7.8))
      (display "pH level is optimal.")
      (display "pH level is not optimal.")))

(define (get-ph-level)
  (display "Enter the current pH level: ")
  (read))

; Call the check-ph-level function to test
;(check-ph-level)