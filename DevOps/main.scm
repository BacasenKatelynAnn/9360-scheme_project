#lang scheme
; main.scm

; Add modules
(require "FeedingTime.scm")
(require "WaterTempModule.scm")
(require "GetpHLevel.scm")

(define (main)
  
  ;FeedingTime module
  (define user-hour (get-current-time))
  (if (is-feeding-time user-hour)
      (displayln "It's feeding time!")
      (displayln "It's not feeding time."))

  ;WaterTempModule module
  (display "Adjusted water temperature: ")
  (display (adjust-temperature))
  (newline)

  ;GetpHLevel module
  (check-ph-level))

(main)
