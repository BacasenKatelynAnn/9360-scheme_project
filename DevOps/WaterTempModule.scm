#lang scheme
;WaterTempModule.scm

(provide get-water-temperature adjust-temperature)

(define (get-water-temperature)
  (display "Enter the current water temperature in Celsius: ")
  (read))

(define (adjust-temperature)
  (define current-temperature (get-water-temperature))
  (if (> current-temperature 26)
      (- 26 current-temperature)
      (- current-temperature 26)))

;(adjust-temperature)