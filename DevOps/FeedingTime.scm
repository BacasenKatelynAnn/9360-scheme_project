#lang scheme
;FeedingTime.scm

(provide get-current-time is-feeding-time)

(define (get-current-time)
  (display "Enter the hour of the day (in military time): ")
  (read))

(define (is-feeding-time hour)
  (or (= hour 8) (= hour 12) (= hour 20)))

; Example:
;(define user-hour (get-current-time))

;(if (is-feeding-time user-hour)
;    (displayln "It's feeding time!")
;    (displayln "It's not feeding time."))