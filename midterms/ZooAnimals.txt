No.			Name				Scientific Name				Given Name			Age			Habitat Enclosure
1			Fox				Vulpes vulpes				Foxy			4			terrestrial
2			J.Fish				Atolla wyvillei				Alex			4			aquatic
3			J.Fish				P. porpita				Sheena			4			aquatic
4			J.Fish				Crossota sp				Sheena			4			aquatic
5			J.Fish				C. colorata				Hannah			3			aquatic
6			J.Fish				C. achlyos				Mik			4			aquatic
7			Tiger				Panthera tigris				Tigger			9			terrestrial
8			Owl				Strigiformes				Eren			5			terrestrial
9			Owl				B. virginianus				Katelyn			7			terrestrial
