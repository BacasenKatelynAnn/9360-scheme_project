#lang scheme

; Makes or init. an empty list to store the animals
(define zoo-animals '()) 

; Prompts for the user to enter the needed data
; User enters specified data based on prompt
(define (add-animal)
  (display "\n")
  (let ((animal (begin (display "Enter animal: ") (read-line)))
        (scientific-name (begin (display "Enter scientific name: ") (read-line)))
        (given-name (begin (display "Enter animal's registered name in a zoo: ") (read-line)))
        (age (begin (display "Enter animal's age: ") (string->number (read-line))))
        (habitat-enclosure (begin (display "Enter animal habitat enclosure (aquatic/terrestrial): ") (read-line))))
    (set! zoo-animals (append zoo-animals `((,animal ,scientific-name ,given-name ,age ,habitat-enclosure))))
    (display "Animal added successfully!\n")))

  ; Saves the data of the animals in a newly generated file
  ; Writes the data of the animals in an existing file
  ; Utilizes the zoo-animals list and checks if its empty
  ; Iterates through the first animal in the list
  ; A table is generated and in each row, the animals are formatted
  ; according to the table header.
(define (save-data-to-file filename)
  (with-output-to-file filename
    (lambda ()
      (display "No.\t\t\tName\t\t\t\tScientific Name\t\t\t\tGiven Name\t\t\tAge\t\t\tHabitat Enclosure\n")
      (let loop ((index 1) (animals zoo-animals))
        (cond
          ((null? animals) '())
          (else
           (let ((animal (car animals)))
             (display (format "~a\t\t\t~a\t\t\t\t~a\t\t\t\t~a\t\t\t~a\t\t\t~a\n" index (car animal) (cadr animal) (caddr animal) (cadddr animal) (car (cddddr animal))))
             (loop (+ index 1) (cdr animals)))))))))
  ; Takes an already existing file to modify the animal contents
  ; Iterates through a row in the file
  ; Reads the rows and takes every animal to be stored to zoo-animals list
  ; eof-object checks if a certain line is the "end" and stops the loop or iteration
  (define (load-data-from-file filename)
    (with-input-from-file filename
      (lambda ()
        (let loop ()
          (let ((line (read-line)))
            (if (eof-object? line)
                (begin
                (display "Data loaded from file successfully!\n") ; Add this line for confirmation
                '())
                (begin
                  ; Parse the line and add the data to the zoo-animals list
                  (set! zoo-animals (append zoo-animals (list (string->list line))))
                  (loop))))))))

    ; Following the system's tabular format, the list of animals stored in
    ; the zoo-animals list are displayed.
    ; Procedure iterates through every single line and keeps track of the indices
    ; The current row/iterated animal is formatted via the table headers:
    ; No. (index or animal number), animal name, scientific name, given name, age, and its habitat enclosure.
    (define (display-animals)
      ;(display "\nNo.\t\tName\tScientific Name\tGiven Name\tAge\tHabitat Enclosure\n")
      (let loop ((index 1) (animals zoo-animals))
        (cond
          ((null? animals) '())
          (else
           (let ((animal (car animals)))
             (display (format "~a\t\t\t~a\t\t\t\t~a\t\t\t\t~a\t\t\t~a\t\t\t~a\n" index (car animal) (cadr animal) (caddr animal) (cadddr animal) (car (cddddr animal))))
             (loop (+ index 1) (cdr animals)))))))
 
    ; Removes or deletes an animal in the zoo-animals list.
    ; The remove-animal procedure automatically calculates the number of animals
    ; stored within the zoo-animals list.
    ; User must enter an existing animal number found in the list to remove that animal.
    ; The animal associated with the specified index or animal number within the zoo-animals list
    ; will be removed and the list is updated accordingly.
    (define (remove-animal index)
      (if (and (integer? index) (<= 1 index (length zoo-animals)))
          (begin
            (set! zoo-animals (remove-item zoo-animals index))
        (display "Animal removed successfully.\n"))
      (display "Invalid index provided. Please give a valid animal number.\n")))
    
    ; remove-item is utilized in the "remove-animal" procedure
    ; This is the logic that handles the removal of an animal
    ; in the zoo-animals list. Given an index, it removes that row.
    ; Given an empty list, it returns the list as is considering that
    ; there is nothing to remove. Upon entering a negative number, the
    ; procedure returns the original list as well.
    (define (remove-item lst n)
      (if (or (null? lst) (<= n 0)) lst
          (if (= n 1) (cdr lst)
              (cons (car lst) (remove-item (cdr lst) (- n 1))))))

(define (main)
  (display "Welcome to the Zoo Animal Database!\n")
  (let loop ()
    (display "\nOptions:\n1. Add an animal\n2. Save data to a file\n3. Load data from a file\n4. Display animals\n5. Remove an animal\n6. Quit\n")
    (display "Enter your option: ") ; Prompt the user to enter their choice
    (let ((choice (read-line)))
      (cond
        ((equal? choice "1") (add-animal))
        ((equal? choice "2")
         (display "Enter the filename to save data to: ")
         (let ((filename (string-append (read-line) ".txt")))
           (save-data-to-file filename)))
        ((equal? choice "3") (load-data-from-file "zoo_animals.txt"))
        ((equal? choice "4") (display-animals))
        ((equal? choice "5")
         (display "Enter the index of the animal to remove: ")
         (let ((index (string->number (read-line))))
           (remove-animal index)))
        ((equal? choice "6") 
         (display "Goodbye!\n")
         (exit))
        (else
          (display "Invalid option. Please choose a valid option.\n")))
    (loop))))

; Run the main program
(main)


      ; Run the main program
      (main)